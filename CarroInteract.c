#include <glut.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>


static float ombro = 0, cotovelo = 0, nuca = 0, pe = 0, mao = 0, angulo = 0, angulo2 = 0, angulo3 = 0, tamx = 0.0 , tamy = 0.0 ,tamz = 0.0, posx = 0.0 , posy = 0.0 , posz = 0.0;


void init(void){
    glClearColor (0.0, 0.0, 0.0, 0.0);
}


void display(void){
    glClear (GL_COLOR_BUFFER_BIT);
    glPushMatrix();
    
    // ----------------------------
    glPushMatrix();
    
    // body
    glRotatef(ombro, 0.0, 1.0, 0.0);
    glRotatef(mao, 1, 0.0, 0.0);
    glTranslated(pe, cotovelo, nuca);
    glScalef(2, 1.0, 1.0);
    glutSolidCube(1);
    
    // front
    glTranslated(0.65, -0.25, 0.0);
    glScalef(0.5, 0.5, 1);
    glutSolidCube(1);
   
    // rear
    glTranslated(-2.8, 0.0, 0.0);
    glScalef(1, 1, 1);
    glutSolidCube(1);  
    
    // roda direita back
    glTranslated(0.0, -0.9, 0.5);
    glScalef(0.4, 0.4, 0.4);
    glutWireTorus(0.2,0.8,16,16);
    
    // roda esquerda back
    glTranslated(0.0, 0.0, -2.5);
    glScalef(1, 1, 1);
    glutWireTorus(0.2,0.8,16,16);
    
    // roda direita front
    glTranslated(7, 0.0, 2.5);
    glScalef(1, 1, 1);
    glutWireTorus(0.2,0.8,16,16);
    
    // roda esquerda front
    glRotatef(angulo, 0.0, 0.0, 1);
    glTranslated(0, 0.0, -2.5);
    glScalef(1, 1, 1);
    glutWireTorus(0.2,0.8,16,16);
        
    // spoiler
    glTranslated(-8, 4.5, 1.25);
    glScalef(1, 0.5, 3);
    glutSolidCube(1); 
    
    // spoiler coisinha
    glPushMatrix();
        glTranslated(-0.2, -1, -0.3);
        glScalef(0.1, 2, 0.1);
        glutSolidCube(1);
    glPopMatrix();
    
    // spoiler coisinha
    glPushMatrix();
        glTranslated(-0.2, -1, 0.3);
        glScalef(0.1, 2, 0.1);
        glutSolidCube(1);
    glPopMatrix();
    
    glPopMatrix();
    // ----------------------------

    
    glPopMatrix();
    glutSwapBuffers();
}


void reshape (int w, int h){
    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective(65.0, (GLfloat) w/(GLfloat) h, 1.0, 20.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef (0.0, 0.0, -5.0);
}


void keyboard (unsigned char key, int x, int y){
    switch (key) {
        
        // rota��o
        case 'Q':
            ombro = ombro + 5;
            glutPostRedisplay();
            break;
        case 'E':
            ombro = ombro - 5;
            glutPostRedisplay();
            break;
        
        // direita e esquerda
        case 'A':
            pe = pe - 0.3;
            glutPostRedisplay();
            break;
        case 'D':
            pe = pe + 0.3;
            glutPostRedisplay();
            break;
            
        // drift para os lados
        case 'W':
            nuca = nuca + 0.3;
            glutPostRedisplay();
            break;
        case 'S':
            nuca = nuca - 0.3;
            glutPostRedisplay();
            break;
        
        
        // rota��o
        case 'q':
            ombro = ombro + 5;
            glutPostRedisplay();
            break;
        case 'e':
            ombro = ombro - 5;
            glutPostRedisplay();
            break;
        
        // direita e esquerda
        case 'a':
            pe = pe - 0.3;
            glutPostRedisplay();
            break;
        case 'd':
            pe = pe + 0.3;
            glutPostRedisplay();
            break;
            
        // drift para os lados
        case 'w':
            nuca = nuca + 0.3;
            glutPostRedisplay();
            break;
        case 's':
            nuca = nuca - 0.3;
            glutPostRedisplay();
            break;
        
            
        case 27:
            exit(0);
            break;
        default:
            break;
    }
}

void mouse(int button, int state, int x, int y){
    if(button == GLUT_LEFT_BUTTON)
        if(state == GLUT_DOWN){
            mao = (mao - 10);
            
        }
    if(button == GLUT_RIGHT_BUTTON)
        if(state == GLUT_DOWN){
            
            mao = (mao + 10);
        }
    glutPostRedisplay();
}

int main(int argc, char** argv){
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize (500, 500);
    glutInitWindowPosition (100, 100);
    glutCreateWindow (argv[0]);
    init ();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
    return 0;
}
